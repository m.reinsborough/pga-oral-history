+++
title = "About"
date = 2020-05-20T21:21:01-04:00
weight = 1
+++

In February 1998, movements from all continents met in Geneva to launch a worldwide coordination of
resistances to the global market and so-called &quot;Free&quot; Trade lead by
big corporations and the World Trade Organization (WTO).

Inspired by the Zapatistas,
[Peoples’ Global Action](https://www.nadir.org/nadir/initiativ/agp/)
linked grassroots social movements across the globe. Rejecting northern-dominated
models of international solidarity, this platform was defined by the [PGA hallmarks](https://www.nadir.org/nadir/initiativ/agp/en/#hallmarks), a
[manifesto](https://www.nadir.org/nadir/initiativ/agp/en/pgainfos/manifest.htm)
and organisational [principles](https://www.nadir.org/nadir/initiativ/agp/cocha/principles.htm).  These became instruments for communication and coordination amongst all those fighting against the destruction of humanity and the planet by capitalism, and for building alternatives.  These documents evolved during subsequesnt conferences, to take a clearer anti-capitalist (not just anti-neoliberal) stand, to avoid confusion with right-wing anti-globalisers and to strengthen the politics around gender.

The network is most well-known for calling for decentralized global days of action against
neoliberalism between 1998 and 2001. During those days, hundreds of thousands of people took to the streets in over 60
countries. Groups involved in PGA have also organized Caravans, regional conferences, two global conferences, workshops and other events in many regions of the world.  One website that has compiled this activity is [here](https://www.nadir.org/nadir/initiativ/agp/en/)

TODAY: YThe stories from this horizontalist, internationalist, direct action movement offer important lessons for today's organisers.  For this reason a PGA oral history project is being set up. Many of
the activists of this earlier period have stories and lessons about solidarity,
about what worked (or didn't), about relationships built across diverse
contexts – tales of failure and success. A loose network of movement
activists/scholars are hoping to facilitate an oral history project to ensure
that these stories and insights aren’t lost.  Volunteers welcome!

For more information and to get involved please contact [pgaoralhistory](mailto:pgaoralhistory@tao.ca).

PGA Research bibliography

[pga en](https://www.nadir.org/nadir/initiativ/agp/en/index.html) | [www.agp.org](https://www.nadir.org/nadir/initiativ/agp/index.html)
